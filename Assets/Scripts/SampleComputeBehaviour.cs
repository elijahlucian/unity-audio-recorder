﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleComputeBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    public ComputeShader shader;
    public RenderTexture texture;
    public Renderer rend;
    public Color color;
    public int randOffset;
    public int textureSize = 512;

    void Start() {
        texture = new RenderTexture(textureSize, textureSize, 24);
        texture.enableRandomWrite = true;
        texture.Create();
        rend = GetComponent<Renderer>();
        rend.enabled = true;

        UpdateTextureFromCompute();
    }

    void UpdateTextureFromCompute() {
        int kernel = shader.FindKernel("CSMain");
        // handle failures

        shader.SetInt("randOffset", (int)(Time.timeSinceLevelLoad * 100));
        shader.SetVector("color", color);

        shader.SetTexture(kernel, "Result", texture);
        shader.Dispatch(kernel, textureSize / 8, textureSize / 8, 1);

        rend.material.SetTexture("_MainTex", texture);
    }

    // Update is called once per frame
    void Update() {
        //if (Input.GetKeyUp(KeyCode.Space))
        UpdateTextureFromCompute();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClipBehavior : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClipmaster clipMaster;

    Renderer renderer;
    float[] bandSamples = new float[512];
    float[] bands = new float[8];
    public float sample;
    public float peak;

    private void Awake() {
        renderer = GetComponent<Renderer>();
        audioSource = GetComponent<AudioSource>();
    }
    
    // Update is called once per frame
    void Update()
    {
        audioSource.GetSpectrumData(bandSamples, 1, FFTWindow.BlackmanHarris);

        //float window = 22050 / bandSamples.Length;

        // split audio bands.
        //for (int i = 0; i < bands.Length; i++) {
        //    // get power of two per i
        //    int upperlim = (int)Mathf.Pow(2, i) * 2;

        //}

        float lo = Mathf.Abs(bandSamples[16]);
        float mid = Mathf.Abs(bandSamples[32]);
        float hi = Mathf.Abs(bandSamples[48]);

        float v =  lo + mid + hi;

        float current = v * 200;

        if (current > peak) {
            peak = current;
        } else {
            peak *= 0.95f;
        }
        float u = peak;

        transform.localScale = audioSource.isPlaying ? new Vector3(1 + u * 0.1f, 1 + u * 0.1f, 1 + u * 0.1f) : new Vector3(1, 1, 1);
        renderer.material.SetColor("_Color", new Color(u, u, u));
    }

    public void SetSource(AudioSource source) {
        //audioSource = source;
    }

    private void OnMouseDown() {
        Debug.Log("i have been violated!");
        if (audioSource.isPlaying) {
            
            clipMaster.StopAll();
        } else {
            
            clipMaster.PlayClip(audioSource);
        }
    }
    private void OnMouseUp() {
        Debug.Log("My sanctity has been restored!");
    }
}

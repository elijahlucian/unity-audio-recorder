﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AlbumLoader : MonoBehaviour
{
    public string albumName;
    public string path;
    public AudioClipmaster clipMaster;

    private void OnMouseDown() {
        Debug.Log("ahhhhh");
        Debug.Log("clipmaster" + clipMaster);
        clipMaster.LoadTrack(albumName);
    }


    public void SetPath(string _path) {
        path = _path;

        string[] splitpath = path.Split('\\');
        albumName = splitpath[splitpath.Length - 1];

        GetComponent<TextMeshPro>().SetText(albumName);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControl : MonoBehaviour
{
    public AudioRecorder.TransportOption transportOption;
    public AudioRecorder recorder;

    private void Awake() {
        recorder = GetComponentInParent<AudioRecorder>();
    }

    private void Update() {

        if (recorder.IsRecording() && name == "Record" || recorder.audioSource.isPlaying && name == "Play" ) {
            transform.Rotate(0, 1.2f, 0, Space.Self);
        }
    }

    private void OnMouseDown() {
       recorder.Transport(transportOption);
    }

    private void OnMouseUp() {
        recorder.Stop();
    }
}

﻿using System;

[Serializable]
public class TwitchChat {
    public string username;
    public string text;
    public int id;
}

[Serializable]
public class Project {
    public string id;
    public string name;
    public string author;
    public string website;
}

[Serializable]
public class Actor {
    // may not need in app?
    public string id;
    public string name;
    public string social;
    public string tagline;
}

[Serializable]
public class Script {
    public string name;
    public string file;
    public string character;
    public string voice;
    public string project;
    public string id;
    public string[] parsed;
}

[Serializable]
public class Lines {
    public int number;
    public string previous;
    public string current;
    public string next;
}

[Serializable]
public class View {
    public Lines lines;
    public Script script;
    public Project project;
    public Actor actor; 
}

[Serializable]
public class State {
    public int take;
    public int line;
    public string project;
    public string actor;
    public string script;
    public bool recording;
    //public TwitchChat[] chat;
}

[Serializable]
public class AppState
{
    public State state;
    public View view;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class AudioRecorder : MonoBehaviour
{
    public enum TransportOption {
        Record,
        Stop,
        Play,
        Pause,
    }

    public string[] devices;
    public string selectedDevice;
    public AudioSource audioSource;
    public AudioClipmaster clipmaster;
    //public AppState appState;
    //public string json;

    public int min;
    public int max;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        devices = Microphone.devices;
        selectedDevice = devices[0];
        Microphone.GetDeviceCaps(selectedDevice, out min, out max);

        //StartCoroutine(GetState());
    }

    IEnumerator GetState() {
        string url = "https://admin.vapsquad.com/api/state";
        UnityWebRequest req = UnityWebRequest.Get(url);

        yield return req.SendWebRequest();

        if(req.isNetworkError || req.isHttpError || req.responseCode != 200) {
            Debug.LogError("Get error: - code: " + req.responseCode);
            yield break;
        }

        string response = req.downloadHandler.text;
        //appState = JsonUtility.FromJson<AppState>(response);
        Debug.Log("Response Code: " + req.responseCode);
        Debug.Log("Response Data: " + response);
    }

    // TODO: advance line
    // TODO: another take
    // TODO: moreshit

    private void OnMouseDown() {
        //StartCoroutine(GetState());
    }

    public bool IsRecording() {
        return Microphone.IsRecording(selectedDevice);
    }

    public void Record() {
        clipmaster.ClearAll();
        AudioClip newClip = Microphone.Start(selectedDevice, false, 10, 44100);
        audioSource.clip = newClip;
    }


    public void Play() {
        audioSource.Play();
    }

    public void Stop() {
        if (IsRecording()) {
            Microphone.End(selectedDevice);

            clipmaster.Spawn(audioSource.clip);
        } else {
            audioSource.Stop();
        }
    }

    public void Transport(TransportOption id) {
        switch (id) {
            case TransportOption.Record:
                Record();
                break;
            case TransportOption.Stop:
                Stop();       
                break;
            case TransportOption.Play:
                Play();
                break;
            default:
                break;
        }
    }

}

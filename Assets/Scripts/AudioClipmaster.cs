﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AudioClipmaster : MonoBehaviour
{
    public GameObject clipPrefab;
    public GameObject songPrefab;
    public AudioSource mixer;
    public GameObject vis;

    public List<GameObject> clips = new List<GameObject>();
    public List<string> songs = new List<string>();

    public string currentFolder;
    public string nowPlaying;
    // array of RecordedClips

    void Awake()
    {
        string[] songpaths = System.IO.Directory.GetDirectories("Assets\\Resources\\Songs");
        Debug.Log(songpaths);

        for (int i = 0; i < songpaths.Length; i++) {
            string path = songpaths[i];
            
            songs.Add(path);

            GameObject song = Instantiate(songPrefab);
            //song.name = path.Split('\\')[path.Split('\\').Length - 1];

            song.GetComponent<AlbumLoader>().clipMaster = this;
            song.GetComponent<AlbumLoader>().SetPath(path);

            float u = (float)i / (songpaths.Length);
            Debug.Log(u);
            song.transform.position = new Vector3(0, -0.5f - u * 1.8f, 0);
        }
    }

    public void LoadTrack(string songName) {
        StopAll();
        currentFolder = songName;
        AudioClip[] tracks = Resources.LoadAll<AudioClip>("Songs\\"+songName);

        ClearAll();
        Debug.Log("Loading tracks " + tracks);
        for (int i = 0; i < tracks.Length; i++) {
            AudioClip track = tracks[i];
            float u = (float)i / (tracks.Length - 1);

            GameObject songClip = Instantiate(clipPrefab);
            songClip.GetComponent<AudioSource>().clip = track;
            songClip.GetComponent<ClipBehavior>().clipMaster = this;
            float x = (u - 0.5f) * 6;
            songClip.transform.position = new Vector3(x, 1, 1);
            Debug.Log(i + ", " + u + ", " + track.name);
            

            songClip.transform.parent = transform;
            clips.Add(songClip);
            Debug.Log("new clip added to clipmaster" + u);
        }
    }

    public void ClearAll() {
        foreach (GameObject clip in clips) {
            Destroy(clip);
        }
        clips = new List<GameObject>();

    }

    void Update()
    {
        // not much 

    }

    public void StopAll() {
        foreach (GameObject clip in clips) {
            clip.GetComponent<AudioSource>().Stop();
        }
    }

    public void PlayClip(AudioSource audioSource) {

        StopAll();
        audioSource.Play();
    }

    public void Spawn(AudioClip _clip) {
        GameObject newClip = Instantiate(clipPrefab);

        newClip.transform.parent = transform;
        newClip.GetComponent<AudioSource>().clip = _clip;

        ClipBehavior clipBehavior = newClip.GetComponent<ClipBehavior>();
        clipBehavior.clipMaster = this;

        float u = (float)clips.Count;
        newClip.transform.position = new Vector3(u, u, u);
        clips.Add(newClip);
        // iterate thru clips and reposition based on size.
    }
}

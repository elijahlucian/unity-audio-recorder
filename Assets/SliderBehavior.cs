﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SliderBehavior : MonoBehaviour
{
    public AudioMixer mixer;    

    private void OnMouseDrag() {
        Vector3 position = transform.position;
        Vector3 relMouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (relMouse.y > 2 || relMouse.y < -2) {
            return;
        }
        float u =  ((relMouse.y + 2) / 4) ;
        mixer.SetFloat("MasterVolume", Mathf.Log10(u) * 20);
        transform.position = new Vector3(position.x, relMouse.y, position.z);
        Debug.Log(relMouse.y);
    }

}
